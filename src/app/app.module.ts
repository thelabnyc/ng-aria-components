import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgAriaAccordionModule } from 'ng-aria-accordion';

@NgModule({
    declarations: [AppComponent],
    imports: [BrowserModule, NgAriaAccordionModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
