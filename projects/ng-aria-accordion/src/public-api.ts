/*
 * Public API Surface of ng-aria-accordion
 */

export {
    AccordionDirective,
    AccordionItemDirective,
    AccordionHeaderDirective,
    AccordionPanelDirective,
} from './lib/ng-aria-accordion.directives';
export * from './lib/ng-aria-accordion.module';
