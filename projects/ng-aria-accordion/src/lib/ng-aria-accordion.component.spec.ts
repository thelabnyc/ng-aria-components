import { render } from '@testing-library/angular';

import { AccordionDirective, NgAriaAccordionModule } from 'ng-aria-accordion';

function renderComponent(otherOptions?: {
    allowMultiple?: boolean;
    allowToggle?: boolean;
}) {
    return render(AccordionDirective, {
        imports: [NgAriaAccordionModule],
        excludeComponentDeclaration: true,
        template: `
            <button (click)="showExtraPanel = true">extra</button>
            <ul
                ariaAccordion
                [allowMultiple]="allowMultiple"
                [allowToggle]="allowToggle"
            >
                <li ariaAccordionItem>
                    <button ariaAccordionHeader>header 1</button>
                    <div ariaAccordionPanel>Panel 1</div>
                </li>
                <li ariaAccordionItem>
                    <button ariaAccordionHeader>header 2</button>
                    <div ariaAccordionPanel>Panel 2</div>
                </li>
                <li *ngIf="showExtraPanel" ariaAccordionItem>
                    <button ariaAccordionHeader>header 3</button>
                    <div ariaAccordionPanel>Panel 3</div>
                </li>
            </ul>
        `,
        componentProperties: otherOptions,
    });
}

describe('Accordion', async () => {
    it('should show the first panel when the first header is clicked', async () => {
        const component = await renderComponent();

        const button = component.getByText('header 1');
        const panel = component.getByText('Panel 1');
        expect(panel.hidden).toBeTruthy();
        component.click(button);
        expect(panel.hidden).toBeFalsy();
    });

    it('should have headers and panels linked by ids and aria attribute', async () => {
        const component = await renderComponent();

        const button = component.getByText('header 1');
        const panel = component.getByText('Panel 1');

        expect(button.id).toMatch(panel.getAttribute('aria-labeledby'));
        expect(panel.id).toMatch(button.getAttribute('aria-controls'));
    });

    it('should allow multiple open panels when allowMultiple is false', async () => {
        const component = await renderComponent({ allowMultiple: true });

        const buttonOne = component.getByText('header 1');
        const panelOne = component.getByText('Panel 1');
        const buttonTwo = component.getByText('header 2');
        const panelTwo = component.getByText('Panel 2');

        expect(panelOne.hidden).toBeTruthy();
        expect(panelTwo.hidden).toBeTruthy();
        component.click(buttonOne);
        expect(panelOne.hidden).toBeFalsy();
        expect(panelTwo.hidden).toBeTruthy();
        component.click(buttonTwo);
        expect(panelOne.hidden).toBeFalsy();
        expect(panelTwo.hidden).toBeFalsy();
    });

    it('should allow only one open panel when allowMultiple is false', async () => {
        const component = await renderComponent({ allowMultiple: false });

        const buttonOne = component.getByText('header 1');
        const panelOne = component.getByText('Panel 1');
        const buttonTwo = component.getByText('header 2');
        const panelTwo = component.getByText('Panel 2');

        expect(panelOne.hidden).toBeTruthy();
        expect(panelTwo.hidden).toBeTruthy();
        component.click(buttonOne);
        expect(panelOne.hidden).toBeFalsy();
        expect(panelTwo.hidden).toBeTruthy();
        component.click(buttonTwo);
        expect(panelOne.hidden).toBeTruthy();
        expect(panelTwo.hidden).toBeFalsy();
    });

    it('should support dynamically added components', async () => {
        const component = await renderComponent();

        expect(() => component.getByText('header 3')).toThrowError();
        expect(() => component.getByText('Panel 3')).toThrowError();

        const addExtraThing = component.getByText('extra');
        component.click(addExtraThing);

        const button = component.getByText('header 3');
        const panel = component.getByText('Panel 3');

        expect(panel.hidden).toBeTruthy();
        component.click(button);
        expect(panel.hidden).toBeFalsy();
    });
});
