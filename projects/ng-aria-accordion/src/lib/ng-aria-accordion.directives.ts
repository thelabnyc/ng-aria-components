import {
    Directive,
    ContentChildren,
    QueryList,
    Input,
    ContentChild,
    HostBinding,
    AfterContentInit,
    EventEmitter,
    HostListener,
    OnDestroy,
    AfterContentChecked,
    isDevMode,
} from '@angular/core';
import { IdService } from './id.service';
import { merge, BehaviorSubject, EMPTY } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Directive({ selector: '[ariaAccordionPanel]' })
export class AccordionPanelDirective implements AfterContentChecked {
    isActive: boolean;
    uid: number;
    @HostBinding('id') get id() {
        if (typeof this.uid !== 'number') return;
        return `Panel:${this.uid}`;
    }
    @HostBinding('attr.aria-labeledby') get ariaLabeledBy() {
        if (typeof this.uid !== 'number') return;
        return `Header:${this.uid}`;
    }

    @HostBinding('hidden') get isHidden() {
        return !this.isActive;
    }
    @HostBinding('attr.role') get role() {
        return 'region';
    }

    private started = 0;
    ngAfterContentChecked() {
        if (this.started > 1) return;
        if (this.started === 1 && typeof this.uid !== 'number') {
            devLog(
                '`accordionPanel` must be rendered inside of an `accordionItem`, and there must not be more than one panel per item',
            );
        }
        this.started += 1;
    }
}

@Directive({ selector: 'button[ariaAccordionHeader]' })
export class AccordionHeaderDirective implements AfterContentChecked {
    isActive: boolean;
    uid: number;
    allowToggle: boolean;
    @HostBinding('id') get id() {
        return `Header:${this.uid}`;
    }
    @HostBinding('attr.aria-controls') get ariaControls() {
        return `Panel:${this.uid}`;
    }

    @HostBinding('attr.aria-expanded') get isExpanded() {
        return this.isActive;
    }
    @HostBinding('attr.aria-disabled') get ariaDisabled() {
        return this.isActive && !this.allowToggle;
    }

    clickEmitter = new EventEmitter();
    @HostListener('click', ['$event']) onClick(event: MouseEvent) {
        this.clickEmitter.emit();
    }

    private started = 0;
    ngAfterContentChecked() {
        if (this.started > 1) return;
        if (this.started === 1 && typeof this.uid !== 'number') {
            devLog(
                '`accordionHeader` must be rendered inside of an `accordionItem`, and there must not be more than one panel per item',
            );
        }
        this.started += 1;
    }
}

@Directive({ selector: '[ariaAccordionItem]' })
export class AccordionItemDirective implements AfterContentInit {
    private _isActive = false;
    get isActive() {
        return this._isActive;
    }
    set isActive(value: boolean) {
        this._isActive = value;
        this.header.isActive = value;
        this.panel.isActive = value;
    }

    set allowToggle(value: boolean) {
        this.header.allowToggle = value;
    }

    @ContentChild(AccordionHeaderDirective, { static: false })
    header!: AccordionHeaderDirective;
    @ContentChild(AccordionPanelDirective, { static: false })
    panel!: AccordionPanelDirective;

    constructor(private idService: IdService) {}

    ngAfterContentInit() {
        const uid = this.idService.getId();

        if (this.header) {
            this.header.uid = uid;
        } else {
            devLog('All Items must contain one header to trigger them');
        }
        if (this.panel) {
            this.panel.uid = uid;
        } else {
            devLog('All Items must contain one panel of content');
        }
    }
}

@Directive({ selector: '[ariaAccordion]' })
export class AccordionDirective implements AfterContentInit, OnDestroy {
    @Input() allowMultiple = true;
    private _allowToggle = true;
    @Input() set allowToggle(value: boolean) {
        this._allowToggle = value;
        if (this.items) {
            this.items.forEach(item => (item.allowToggle = value));
        }
    }
    get allowToggle() {
        return this._allowToggle;
    }

    @Input() defaultIndex: number;

    @ContentChildren(AccordionItemDirective, { descendants: true })
    items!: QueryList<AccordionItemDirective>;

    itemClicks = new BehaviorSubject<QueryList<AccordionItemDirective> | null>(
        null,
    );

    toggleItem = (index: number) => {
        this.items.forEach((item, i) => {
            if (i === index) {
                if (!item.isActive || this.allowToggle) {
                    item.isActive = !item.isActive;
                }
            } else if (!this.allowMultiple) {
                item.isActive = false;
            }
        });
    };

    activeItemSubscription = this.itemClicks
        .pipe(
            switchMap(items => {
                if (items) {
                    return merge(
                        ...items.map((item, i) =>
                            item.header.clickEmitter.pipe(map(() => i)),
                        ),
                    );
                } else {
                    return EMPTY;
                }
            }),
        )
        .subscribe(this.toggleItem);

    ngAfterContentInit() {
        if (!this.allowToggle && this.allowMultiple) {
            devLog('allowToggle=false and allowMultiple=true are incompatible');
        }

        this.items.forEach((item, i) => {
            item.allowToggle = this.allowToggle;
            if (typeof this.defaultIndex === 'number') {
                if (this.defaultIndex === i) {
                    item.isActive = true;
                }
            }
        });

        this.itemClicks.next(this.items);
        this.items.changes.subscribe(this.itemClicks);
    }

    ngOnDestroy() {
        if (this.activeItemSubscription) {
            this.activeItemSubscription.unsubscribe();
        }
    }
}

function devLog(message: string) {
    if (isDevMode()) {
        console.warn(`Accordion: ${message}.`);
    }
}
