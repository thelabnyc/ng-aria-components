import { Injectable } from '@angular/core';

let nextId = 0;

@Injectable()
export class IdService {
    getId() {
        return (nextId += 1);
    }
}
