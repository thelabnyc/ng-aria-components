import { NgModule } from '@angular/core';
import {
    AccordionDirective,
    AccordionItemDirective,
    AccordionPanelDirective,
    AccordionHeaderDirective,
} from './ng-aria-accordion.directives';
import { IdService } from './id.service';

@NgModule({
    declarations: [
        AccordionDirective,
        AccordionItemDirective,
        AccordionPanelDirective,
        AccordionHeaderDirective,
    ],
    imports: [],
    exports: [
        AccordionDirective,
        AccordionItemDirective,
        AccordionPanelDirective,
        AccordionHeaderDirective,
    ],
    providers: [IdService],
})
export class NgAriaAccordionModule {}
